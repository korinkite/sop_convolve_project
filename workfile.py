#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A temp file for sending scene layouts to Houdini.'''

# IMPORT STANDARD LIBRARIES
import textwrap

# IMPORT THIRD-PARTY LIBRARIES
import hou


def _get_move_code():
    '''str: Create the code which will be used to move our curve points.'''
    # This assumes that the incoming geometry has 7 points
    return textwrap.dedent(
        '''\
        node = hou.pwd()

        positions = [
            (0, 0, 0),
            (0, 0.166667, 0),
            (0, 0.333333, 0.100889),
            (0, 0.5, 0.358052),
            (0.0472074, 0.689328, 0.608379),
            (0, 0.833333, 0.74418),
            (0, 1, 0.845737),
        ]

        for point, position in zip(node.geometry().points(), positions):
            point.setPosition(position)
        ''')


def create_basic_scene():
    '''Create a basic curve for testing.'''
    geo = hou.node('/obj').createNode('geo')
    geo.node('file1').destroy()  # Remove the default node

    line = geo.createNode('line')
    line.parm('points').set(4)
    line.parm('dirx').set(0.45)
    line.parm('diry').set(0.45)
    line.parm('dirz').set(0)

    # edit = geo.createNode('python')
    # edit.parm('python').set(_get_move_code())

    # resample = geo.createNode('resample')
    # resample.parm('treatpolysas').set(1)  # 1 = "Subdivision Curves"
    # resample.parm('length').set(0.02)  # Make this line very smooth

    frame = geo.createNode('polyframe')
    frame.parm('Non').set(False)  # Turn off normals
    frame.parm('tangentuon').set(True)
    frame.parm('tangentu').set('convolve_tangent')
    frame.parm('tangentvon').set(True)
    frame.parm('tangentv').set('convolve_bitangent')

    texture = geo.createNode('texture')
    texture.parm('type').set(3)  # Rows & Columns
    texture.parm('coord').set(1)  # Point

    convolve = geo.createNode('hdk_convolve')

    out = geo.createNode('null')
    out.setDisplayFlag(True)

    # edit.setInput(0, line)
    # resample.setInput(0, edit)
    # frame.setInput(0, resample)
    frame.setInput(0, line)
    texture.setInput(0, frame)
    convolve.setInput(0, texture)
    out.setInput(0, convolve)

    # Make the network layout pretty. Optional for debugging
    geo.layoutChildren(geo.children())

    return out


def main():
    '''The main execution of the current script.'''
    return create_basic_scene()


if __name__ == '__main__':
    main()
