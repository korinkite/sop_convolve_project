nnoremap <leader>o :TmuxRight houdini ../visualize.py &<CR>
nnoremap <leader>r :TmuxRight hython ../workfile.py<CR>
nnoremap <leader>x :VimdiniFileSend ../workfile.py<CR>

" Run Houdini in a debug process
nnoremap <leader>d :ConqueGdb /opt/hfs16.5/bin/houdini-bin -ex "run -foreground" -ex bload<CR>
