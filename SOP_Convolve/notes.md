## Todo

- Get translation to work

- Figure out how to get a tangent/bi-tangent from PolyFrame and get that
  equation to work


- Make it so that the convolve node DOES NOT make any unnecessary attributes if
  they don't already exist(?)
   - Maybe people would want to keep that data, to query from it? Ask Cole


- Read point data from the incoming curve (input)

- Get the tangent/bi-tangent and apply it as attribute data (if it does not
  exist already as data)
- Create the formula
 - Apply the attribute info and tangent/bi-tangent info to the nodes
- Implement "move" semantics


http://forums.odforce.net/topic/18511-how-can-i-calculate-curve-normal/
https://www.sidefx.com/forum/topic/22762/
https://bitbucket.org/alexxbb/hdk_pathdeformer/src/cbf07e9aa15ff796b01e105d56dda77f4cec7a38/sop_pathdeform.cpp?at=default


## Things that must be built in order to create the node
--- Create a cmake project with 2 DSOs as output
--- Create a node which has the interface that's the same as Cole's
--- Create a node that takes input
 --- Use default values if none are given

```
matrix m_local = ident();
m_local *= m_r;
m_local *= m_t;
new_p = set(m_local.ax, m_local.ay, m_local.az);
@P = new_p;
```



## Questions to answer
- What does `_sh` do?

- What is PRM_Template and PRM_TemplateBuilder? Look into those
- What is the `_sh` all about?
    - Example: 

    ```cpp
    static PRM_TemplateBuilder templ("SOP_Star.C"_sh, theDsFile);
    ```
- What does the `virtual` keyword do in C++ again? And what is the significance
  of a virtual destructor?

- I didn't need to include this? Why not?

```cpp
// The static member variable definition has to be outside the class definition.
// The declaration is inside the class.
const SOP_NodeVerb::Register<SOP_FooVerb> SOP_FooVerb::verb;
```

- Figure out OP_Operator's flags

- What happens when you make a destructor virtual?

- What does public inheritance do?
Example:
    class SOP_ConvolveVerb : public SOP_NodeVerb {

vs 

    class SOP_ConvolveVerb : SOP_NodeVerb {



Why does this work



```
static PRM_Name names[] = {
    PRM_Name {"width", "Width"},
};


PRM_Template SOP_Convolve_Config::template_list[] = {
    PRM_Template {
        PRM_FLT,
        1,
        &names[0],
        PRMoneDefaults,
        0,
        &PRMrolloffRange,
    },
}
```

But not this?

```
static const WIDTH_NAME {"width", "Width"};

PRM_Template SOP_Convolve_Config::template_list[] = {
    PRM_Template {
        PRM_FLT,
        1,
        &WIDTH_NAME,
        PRMoneDefaults,
        0,
        &PRMrolloffRange,
    },
}
```


## Useful forum discussions
http://forums.odforce.net/topic/3676-get-value-of-parameter/
http://forums.odforce.net/topic/21325-create-point-attribute-outside-cookmysop/
http://forums.odforce.net/topic/19577-get-all-the-available-point-attributes/
http://forums.odforce.net/topic/18511-how-can-i-calculate-curve-normal/

## Useful example code
https://bitbucket.org/alexxbb/hdk_pathdeformer/src/cbf07e9aa15ff796b01e105d56dda77f4cec7a38/sop_pathdeform.cpp?at=default
https://bitbucket.org/alexxbb/hdk_pathdeformer/raw/cbf07e9aa15ff796b01e105d56dda77f4cec7a38/sop_pathdeform.cpp?at=default
