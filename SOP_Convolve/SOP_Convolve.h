#pragma once

// IMPORT THIRD-PARTY LIBRARIES
#include <SOP/SOP_Node.h>

class SOP_Convolve : public SOP_Node
{
    public:
        static const UT_StringHolder type_name;
        static PRM_Template template_list[];

        static OP_Node *init(OP_Network *net, const char *name, OP_Operator *op)
        {
            return new SOP_Convolve(net, name, op);
        }

    protected:
        SOP_Convolve(OP_Network *net, const char *name, OP_Operator *op) : SOP_Node(net, name, op) { }

        virtual ~SOP_Convolve() {}

        virtual OP_ERROR cookMySop(OP_Context &context);

    private:
        // TODO: Make this a set of common functions so that it can be shared
        // with `hdk_convolve_config` and `hdk_convolve_config`
        //
        float get_parameter_cycles(fpreal time) {
            return evalFloat("cycles", /* component index= */ 0, time);
        }
        float get_parameter_cycle_by_size(fpreal time) {
            return static_cast<bool>(evalInt("cycle_by_size", /* component index= */ 0, time));
        }
        float get_parameter_threads(fpreal time) {
            return evalInt("threads", /* component index= */ 0, time);
        }
        float get_parameter_rotate(fpreal time) {
            return evalFloat("rotate", /* component index= */ 0, time);
        }

        float get_parameter_width(fpreal time) {
            return evalFloat("width", /* component index= */ 0, time);
        }

        void build_curve_attributes(GU_Detail *curve_gdp, const GEO_Curve *curve_prim, fpreal time);
};
