#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT STANDARD LIBRARIES
import unittest

# IMPORT THIRD-PARTY LIBRARIES
import hou

# IMPORT 'LOCAL' LIBRARIES
import common_test


class ConvolveConfigTestCase(unittest.TestCase):

    '''Test various "Convolve Config" node settings.'''

    def test_override_options(self):
        '''Make sure that convolve settings get overridden by default.'''

    def test_no_override_options(self):
        '''Enable an option to passthrough convolve settings if they exist.'''
