#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A set of common functions used for integration testing.'''

# IMPORT STANDARD LIBRARIES
import textwrap

# IMPORT THIRD-PARTY LIBRARIES
import hou


def _get_move_code():
    '''str: Create the code which will be used to move our curve points.'''
    # This assumes that the incoming geometry has 7 points
    return textwrap.dedent(
        '''\
        node = hou.pwd()

        positions = [
            (0, 0, 0),
            (0, 0.166667, 0),
            (0, 0.333333, 0.100889),
            (0, 0.5, 0.358052),
            (0.0472074, 0.689328, 0.608379),
            (0, 0.833333, 0.74418),
            (0, 1, 0.845737),
        ]

        for point, position in zip(node.geometry().points(), positions):
            point.setPosition(position)
        ''')


def create_basic_scene():
    '''Create a basic curve for testing.'''
    geo = hou.node('/obj').createNode('geo')
    geo.node('file1').destroy()  # Remove the default node

    line = geo.createNode('line')
    line.parm('points').set(7)

    edit = geo.createNode('python')
    edit.parm('python').set(_get_move_code())

    resample = geo.createNode('resample')
    resample.parm('treatpolysas').set(1)  # 1 = "Subdivision Curves"

    edit.setInput(0, line)
    resample.setInput(0, edit)
    resample.setDisplayFlag(True)

    # Make the network layout pretty. Optional for debugging
    geo.layoutChildren(geo.children())
