#pragma once

// IMPORT THIRD-PARTY LIBRARIES
#include <SOP/SOP_Node.h>


class SOP_Convolve_Config : public SOP_Node {
    public:
        static const UT_StringHolder type_name;
        static PRM_Template template_list[];

        static OP_Node *init(OP_Network *net, const char *name, OP_Operator *op) {
            return new SOP_Convolve_Config(net, name, op);
        }

    protected:
        SOP_Convolve_Config(OP_Network *net, const char *name, OP_Operator *op) : SOP_Node(net, name, op) { }

        virtual ~SOP_Convolve_Config() {}

        virtual OP_ERROR cookMySop(OP_Context &context);

    private:
        void init_attributes(GU_Detail *geometry, OP_Context const &context);

        // TODO: Make these common functions
        float get_cycles(fpreal time) {
            return evalFloat("cycles", /* component index= */ 0, time);
        }

        float get_cycle_by_size(fpreal time) {
            return static_cast<bool>(evalInt("cycle_by_size", /* component index= */ 0, time));
        }

        int get_inheritance_mode(fpreal time) {
            // TODO: Check if I can return this as a string, instead!
            return evalInt("inheritance", /* component index = */ 0, time);
        }

        float get_threads(fpreal time) {
            return evalInt("threads", /* component index= */ 0, time);
        }

        float get_rotate(fpreal time) {
            return evalFloat("rotate", /* component index= */ 0, time);
        }

        float get_width(fpreal time) {
            return evalFloat("width", /* component index= */ 0, time);
        }
};
