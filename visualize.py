#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A module to help initialize a convolve node in a Houdini scene.'''

# IMPORT THIRD-PARTY LIBRARIES
import hou

# IMPORT THIRD-PARTY LIBRARIES
from vimrpyc.servers import hrpyc
import hdefereval

# IMPORT 'LOCAL' LIBRARIES
import workfile


def _main():
    '''Create the node setup and step into it.

    Also, open a port so we can keep sending commands to Houdini.

    '''
    try:
        hrpyc.start_server()
    except Exception:
        # The address is probably already in use
        pass

    out_node = workfile.main()
    out_node.setCurrent(True, True)


def main():
    '''Create the node setup and step into it.

    Also, open a port so we can keep sending commands to Houdini.

    '''
    # Because we are setting Houdini's viewport, we need to wait until the GUI
    # is fully loaded before executing. That's why we `executeDeferred`, here.
    #
    hdefereval.executeDeferred(_main)


if __name__ == '__main__':
    main()
